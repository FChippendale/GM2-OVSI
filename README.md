# Asmami gas adaptors 

This project began in the Department of Engineering, University of Cambridge and is now being rolled out more widely. 

All documentation is given in our Wiki, selectable from the left hand side bar on this page

# Authors

Akshat Sharma, Alexander Wray, Felix Chippindale and Igor Sterner (alphabetical order only)

{as2992, ajmw4, fc494, is473}@cam.ac.uk

# Acknowledgment

Deep appreciation is given to Professor Alexander Kabla for his organisation, Ben Moore for his support as our partner, Dr. Lara Allen for her continued support with the Centre for Global Equality and Dr. Kobus Preller for his clinical support

# Contact

Reach out to Igor Sterner at is473@cam.ac.uk to find out how to connect with this project

# License

This project is open-source with the intension improving access to medical equipment across the world. None of the ideas presented in Wiki have been medically approved and are **not** safe for clinical use. Any of the ideas on the wiki may be used without restriction for non-commercial use
